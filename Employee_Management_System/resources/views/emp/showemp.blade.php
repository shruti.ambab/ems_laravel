@extends("layout")
@section("title","Employee Details")
@section("content")
<div class="container" style="margin-top:5em;margin-bottom:5em;">
    <div style="float:left"><h4>Employee (ID-{{ $output["emp_id"] }}) Details</h4></div>
    <div class="clearfix"></div>
    <br>
    <ul class="list-group">
        <li class="list-group-item "><img src="{{asset('images/'.$output['frontprofilepic'])}}" style="width:120px;height:120px;" alt="{{ $output['frontprofilepic'] }}"/></li>
        <li class="list-group-item "><b>ID : </b>{{ $output["emp_id"] }}</li>
        <li class="list-group-item"><b>Name : </b>{{ $output["fname"] }} {{ $output["lname"] }}</li>
        <li class="list-group-item"><b>Organization Name : </b>{{ $output["org_name"] }}</li>
        <li class="list-group-item"><b>Email ID : </b>{{ $output["email"] }}</li>
        <li class="list-group-item"><b>Contact : </b>{{ $output["contact"] }}</li>
        <li class="list-group-item"><b>Address : </b>{{ $output["local_add"] }}, {{ $output["city_name"] }}, {{ $output["state_name"] }}, {{ $output["country_name"] }}</li>
        <li class="list-group-item"><b>Employee Status : </b><?php if($output["is_enabled"]==1){echo "Enabled";}else{echo "Disabled";}?></li>
    </ul><br>
    <div>
        <a href="/employees/{{ $output['emp_id'] }}/edit" style="margin-bottom:1em" type=button class="btn btn-success new">Edit</a>
        <form action="/employees/{{ $output['emp_id'] }}" method="post">
            <input class="btn btn-danger" type="submit" value="Delete" />
            @method('delete')
            @csrf
        </form>
    </div>
</div>
@endsection