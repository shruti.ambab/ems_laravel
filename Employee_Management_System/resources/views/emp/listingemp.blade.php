@extends("layout")
@section("title","EMS | Manage Employees")
@section("csslinks")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section("content")
<div style="margin-top:5em;margin-bottom:5em">
    <div ><h4>Manage Employees</h4></div>
    <hr>
    <button type="button" class="btn btn-dark" id="searchbtn">Search with Filter <i class="fa fa-filter"></i></button>
    <a href="/employees/create" type=button class="btn btn-primary new pull-right" style="float:right">Add New Employee</a>
    <div class="clearfix"></div>
    <br>  
    <form class="form-inline" id="filterform" action="/employees/search">
    {{ csrf_field() }}
        <div id="filter" class="d-flex justify-content-between flex-wrap" style="display: none !important;border: 1px solid #000;padding: 20px;border-radius: 20px">
            <div style="margin-bottom:1rem">
                <span>Employee ID : </span>
                <input type="number" min="0" name="emp_id" id="emp_id">
            </div>
            <div >
                <span>First Name : </span>
                <input type="text" name="fname" id="fname">
            </div>
            <div>
                <span>Last Name : </span>
                <input type="text" name="lname" id="lname">
            </div>
            <div style="margin-bottom:1rem">
                <span>Organization : </span>
                <select id ="org_id" name ="org_id">
                    <option value="" selected='true'>Select Organization </option>
                    @foreach($orglist as $value)
                        <option value="{{ $value['org_id'] }}">{{ $value['org_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <span>Email ID : </span>
                <input type="email" name="email" id="email">
            </div>
            <div>
                <span>Contact Number : </span>
                <input type="text" name="contact" id="contact">
            </div>
            <div style="margin-bottom:1rem">
                <span>Local Address : </span>
                <input type="text" name="local_add" id="local_add">
            </div>
            <div>
                <span>Country : </span>
                <select id ="country_id" name ="country_id" >
                    <option value="" selected='true'>Select Country</option>
                    @foreach($countrylist as $value)
                        <option id="{{ $value['country_id'] }}" value="{{ $value['country_id'] }}">{{ $value['country_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <span>State : </span>
                <select id ="state_id" name ="state_id" >
                    <option value="" selected='true'>Select State</option>
                    @foreach($statelist as $value)
                        <option id="{{ $value['state_id'] }}" value="{{ $value['state_id'] }}">{{ $value['state_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <span>City : </span>
                <select id ="city_id" name ="city_id">
                    <option value="" selected='true'>Select City</option>
                    @foreach($citylist as $value)
                        <option id="{{ $value['city_id'] }}" value="{{ $value['city_id'] }}">{{ $value['city_name'] }}</option>
                    @endforeach
                </select>
            </div>   
            <div>
            <span>Employee Status (Enabled or not) :</span>
                <select id ="is_enabled" name ="is_enabled">
                    <option value="1" selected="true">Enabled</option>
                    <option value="0">Disabled</option>
                </select>
            </div> 
            <div>
                <input type="submit" id="searchsubmit" name="searchsubmit" class="btn btn-secondary" value="Search"/>
            </div>
        </div>
    </form>       
    @if($message=Session::get('success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
    @endif
    @if($message=Session::get('error'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
    @endif
    <table class="table table-bordered table-hover" id="listingemp">
        <thead >
            <tr class="table-primary">
                <th onclick="sortTable(0)">Employee ID</th>  
                <th>Profile Picture</th> 
                <th onclick="sortTable(1)">Employee Name</th>
                <th onclick="sortTable(2)">Organization Name</th>
                <th onclick="sortTable(3)">Email ID</th>
                <th onclick="sortTable(4)">Contact Number</th>
                <th onclick="sortTable(5)">Address</th>
                <th>Employee Status</th> 
                <th>Action</th>
            </tr>
        </thead>
        <tbody id="listingempdata">
        <?php foreach($data as $items) : ?>
        @if($items["is_deleted"]=="0")
            <tr>         
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["emp_id"]; ?></td> 
                @if(!(empty($items["frontprofilepic"])))
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"> <img src="{{asset('images/'.$items['frontprofilepic'])}}" style="width:70px;height:70px;" alt="{{ $items['frontprofilepic'] }}"/></td> 
                @else
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';">No Pic Added</td> 
                @endif
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["fname"]." ".$items["lname"]; ?></td>
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["org_name"]; ?></td>
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["email"]; ?></td>
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["contact"]; ?></td>
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php echo $items["local_add"].", ".$items["city_name"].", ".$items["state_name"].", ".$items["country_name"]; ?></td>
                <td onclick="window.location='/employees/{{ $items['emp_id'] }}';"><?php if($items["is_enabled"]==1){echo "Enabled";}else{echo "Disabled";} ?></td>
                <td>
                <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose
                    </button>
                    <div class="dropdown-menu">
                        <div class="dropdown-item">
                            <a href="/employees/{{ $items['emp_id'] }}/edit" class="btn btn-default">Edit</a>
                        </div>
                        <form class="dropdown-item" action="/employees/{{ $items['emp_id'] }}" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                </div>
                </td>
            </tr>
            @endif
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="d-flex justify-content-center">{{$data->appends(Request::except('page'))->links()}}</div>
@endsection
@section("scriptlinks")
<script type="text/javascript" src="{{ URL::asset('js/listingemp.js') }}"></script>
@endsection
