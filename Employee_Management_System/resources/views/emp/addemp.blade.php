@extends("layout")
@section("title","EMS | Add Employee")
@section("csslinks")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section("content")
<div class="container" style="margin-top:5em; margin-bottom:5em">
    <form method="POST" action="/employees" enctype="multipart/form-data">
    {{ csrf_field() }}
    <fieldset class="fieldset">
        <legend class="legend"><span>Add New Employee</span></legend>
        <hr>
        @if($message=Session::get('success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
        @endif
        @if($message=Session::get('error'))
                <div class="alert alert-danger alert-block">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong> {{ $message }} </strong>
                </div>
        @endif
        <div class="form-group">
            <label >First Name :</label>
            <input type="text" name="fname" id="fname" class="form-control" placeholder="Enter Employee First Name" value="{{ old('fname')}}">
            <div id="fnamemsg">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label >Last Name :</label>
            <input type="text" name="lname" id="lname" class="form-control" placeholder="Enter Employee Last Name" value="{{ old('lname')}}">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
                <label >Organization :</label>
                <select class="form-control" id ="orgid" name ="orgid">
                    <option value="" selected='true'>Select Organization </option>
                    @foreach($orglist as $value)
                        <option value="{{ $value['org_id'] }}">{{ $value['org_name'] }}</option>
                    @endforeach
                </select>
            </div>
        <div class="form-group">
            <label >Email ID :</label>
            <input type="text" name="emailid" id="emailid" class="form-control" placeholder="Enter Employee Email ID" value="{{ old('emailid')}}">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label >Contact :</label>
            <input type="text" name="contact" id="contact" class="form-control" placeholder="Enter Employee Contact Number" value="{{ old('contact')}}">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <div class="form-group">
                <label >Address :</label>
                <input type="text" name="localadd" id="localadd" class="form-control" placeholder="Enter Employee Local address" value="{{ old('localadd')}}">
            </div>
            <div class="form-group">
                <label >Country :</label>
                <select class="form-control" id ="countryid" name ="countryid" >
                    <option value="" selected='true'>Select Country</option>
                    @foreach($countrylist as $value)
                        <option id="{{ $value['country_id'] }}" value="{{ $value['country_id'] }}">{{ $value['country_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >State :</label>
                <select class="form-control" id ="stateid" name ="stateid" >
                    <option value="" selected='true'>Select State</option>
                    @foreach($statelist as $value)
                        <option id="{{ $value['state_id'] }}" value="{{ $value['state_id'] }}">{{ $value['state_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >City :</label>
                <select class="form-control" id ="cityid" name ="cityid">
                    <option value="" selected='true'>Select City</option>
                    @foreach($citylist as $value)
                        <option id="{{ $value['city_id'] }}" value="{{ $value['city_id'] }}">{{ $value['city_name'] }}</option>
                    @endforeach
                </select>
            </div>   
        </div>
        <div class="form-group">
            <label>Employee Status (Enabled or not) :</label>
            <select class="form-control" id ="status" name ="status">
                <option value="1" selected="true">Enabled</option>
                <option value="0">Disabled</option>
            </select>
        </div>
        <div class="form-group">
            <label style="margin-bottom:0">Add profile pictures :</label><br>
            <small style="font-size:14px">You can choose mutiple images. Maximum 3 allowed. First picture will be set as avatar by default.</small><br>
            <input style="margin-top:0.6rem" id="profilepics" type="file" name="profilepics[]" accept=".jpg,.jpeg,.png" multiple ><br>
            <input id="frontpicfile" type="hidden" name="frontpicfile" value="frontpicfile"/>
            <small>Only .jpg, .png, .jpeg allowed</small>
            <!-- <div style="margin-top:0.6rem" class="checkbox">
                <label>
                    <input type="checkbox"> Set as Avatar
                </label>
            </div> -->
            <div id="filemsg"><!--   --></div>
            <!-- <div id="uploads"></div> -->
            <!-- <input style="margin-top:0.6rem" type='button' class="btn btn-secondary" name="add" id="add" value='Add Image'> -->
            <!-- <button style="margin-top:0.6rem" type="button" class="btn btn-warning" name="reset" id="reset"><i class="fa fa-history"></i>Clear and Choose again</button> -->
        </div>
        <button style="margin-top:0.5rem" type="submit" name="save" id="save" class="btn btn-primary"><i class="fa fa-upload"></i> Save Data and Upload Images</button>
    </fieldset>
    </form>
</div>
@endsection
@section("scriptlinks")
<script type="text/javascript" src="{{ URL::asset('js/addemp.js') }}"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.js"></script> -->
@endsection

<!-- <script type="text/javascript">
    $( document ).ready(function() {
        $('#countryid').change(function(){
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
            type:"GET",
            url:"{{url('get-state-list')}}?country_id="+countryID,
            alert(url);
            success:function(res){               
                if(res){
                    $("#stateid").empty();
                    $("#stateid").append('<option>Select</option>');
                    $.each(res,function(key,value){
                        $("#stateid").append('<option value="'+key+'">'+value+'</option>');
                    });
            
                }else{
                $("#stateid").empty();
                }
            }
            });
        }else{
            $("#stateid").empty();
            $("#stateid").empty();
        }      
        });
    });
</script> -->