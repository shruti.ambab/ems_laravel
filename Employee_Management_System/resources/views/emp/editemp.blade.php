@extends("layout")
@section("title","EMS | Edit Employee")
@section("csslinks")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section("content")
<div class="container" style="margin-top:5em; margin-bottom:5em">
    <form method="POST" action="/employees/{{ $empdata['emp_id'] }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field("PUT") }}
    <fieldset class="fieldset">
        <legend class="legend"><span>Edit Employee (ID-{{ $empdata["emp_id"] }})</span></legend>
        <hr>
        @if($message=Session::get('success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
        @endif
        @if($message=Session::get('error'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
        @endif
        <div class="form-group">
            <label >First Name :</label>
            <input type="text" name="fname" class="form-control" value="{{ $empdata['fname'] }}">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label >Last Name :</label>
            <input type="text" name="lname" class="form-control" value="{{ $empdata['lname'] }}">
        </div>
        <div class="form-group">
                <label >Organization :</label>
                <select class="form-control" id ="orgid" name ="orgid">
                    <option value="">Select Organization</option>
                    @foreach($orglist as $value)
                    @if($empdata['org_id'] == $value['org_id'])
                        <option value="{{ $value['org_id'] }}" selected="true">{{ $value['org_name'] }}</option>
                    @else
                        <option value="{{ $value['org_id'] }}">{{ $value['org_name'] }}</option>

                    @endif
                    @endforeach
                </select>
            </div>
        <div class="form-group">
            <label >Email ID :</label>
            <input type="text" name="emailid" class="form-control" value="{{ $empdata['email'] }}">
        </div>
        <div class="form-group">
            <label >Contact :</label>
            <input type="text" name="contact" class="form-control" value="{{ $empdata['contact'] }}">
        </div>
        <div class="form-group">
            <div class="form-group">
                <label >Address :</label>
                <input type="text" name="localadd" class="form-control" value="{{ $empdata['local_add'] }}">
            </div>
            <div class="form-group">
                <label >Country :</label>
                <select class="form-control" id ="countryid" name ="countryid" >
                    <option value="">Select Country</option>
                    @foreach($countrylist as $value)
                    @if($empdata['country_id'] == $value['country_id'])
                        <option value="{{ $value['country_id'] }}" selected="true">{{ $value['country_name'] }}</option>
                    @else
                        <option value="{{ $value['country_id'] }}">{{ $value['country_name'] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >State :</label>
                <select class="form-control" id ="stateid" name ="stateid" >
                    <option value="">Select State</option>
                    @foreach($statelist as $value)
                    @if($empdata['state_id'] == $value['state_id'])
                        <option value="{{ $value['state_id'] }}" selected="true">{{ $value['state_name'] }}</option>
                    @else
                        <option value="{{ $value['state_id'] }}">{{ $value['state_name'] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label >City :</label>
                <select class="form-control" id ="cityid" name ="cityid">
                    <option value="">Select City</option>
                    @foreach($citylist as $value)
                    @if($empdata['city_id'] == $value['city_id'])
                        <option value="{{ $value['city_id'] }}" selected="true">{{ $value['city_name'] }}</option>
                    @else
                        <option value="{{ $value['city_id'] }}">{{ $value['city_name'] }}</option>
                    @endif
                    @endforeach
                </select>
            </div>   
        </div>
        <div class="form-group">
            <label>Employee Status (Enabled or not) :</label>
            <select class="form-control" id ="enability" name ="status">
            @if($empdata["is_enabled"]==1){
                <option value="1" selected="true">Enabled</option>
                <option value="0">Disabled</option>
            }
            @elseif($empdata["is_enabled"]==0){
                <option value="0" selected="true">Disabled</option>
                <option value="1">Enabled</option>
            }
            @endif   
            </select>
        </div>
        <div class="form-group">
            <label style="margin-bottom:0.3rem">Profile pictures :</label><br>
            @if($empdata['picscount']!=0)
            <div style="margin-top:0.6rem;margin-bottom:0.6rem" class="checkbox">
                <?php $i=1; ?>
                @foreach($empdata['profilepics'] as $pic)
                    <input type="checkbox" id="pic<?php echo $i;?>" name="pic<?php echo $i;?>"> <img id="picture<?php echo $i;?>" name="picture<?php echo $i;?>" src="{{asset('images/'.$pic)}}" style="width:70px;height:70px;" alt="{{ $pic }}"/>
                    <?php $i++; ?>
                @endforeach 
            </div> 
            @else
                <div>--No profile pics added--<div>
            @endif
            <input id="picscount" type="hidden" name="picscount" value="{{ $empdata['picscount'] }}"/> 
            <input id="deletefiles" type="hidden" name="deletefiles" value="" multiple/>
            <div  style="margin-bottom:0.6rem" id="filemsg"></div>
            <button type='button' style="margin-right:0.5rem" class="btn btn-danger" name="delete" id="delete"><i class="fa fa-trash"></i> Delete </button>
            <button type='button' style="margin-right:0.5rem" class="btn btn-dark" name="avatar" id="avatar"><i class="fa fa-user"></i> Set as Avatar </button>
            <button type='button' style="margin-right:0.5rem" class="btn btn-secondary" name="add" id="add"> Add More </button>             
        <div>
        <div style="margin-top:0.6rem;display:none" id="upload">   
            <small style="font-size:17px;margin-bottom:0">You can choose mutiple images. Maximum 3 allowed. First picture will be set as avatar by default.</small><br>
            <input style="margin-top:0.6rem" id="profilepics" type="file" name="profilepics[]" accept=".jpg,.jpeg,.png" multiple ><br>
            <input id="frontpicfile" type="hidden" name="frontpicfile" value="{{ $empdata['frontprofilepic'] }}"/>
            <small>Only .jpg, .png, .jpeg allowed</small>
        </div>
        <div id="uploadfilemsg"><!--   --></div>
            <!-- <div style="margin-top:0.6rem" class="checkbox">
                <label>
                    <input type="checkbox"> Set as Avatar
                </label>
            </div> -->
            <!-- <div id="filemsg"></div> -->
            <!-- <div id="uploads"></div> -->
            <!-- <input style="margin-top:0.6rem" type='button' class="btn btn-secondary" name="add" id="add" value='Add Image'> -->
            <!-- <button style="margin-top:0.6rem" type="button" class="btn btn-warning" name="reset" id="reset"><i class="fa fa-history"></i>Clear and Choose again</button> -->
        <button style="margin-top:1.5rem" type="submit" name="save" id="save" class="btn btn-primary"><i class="fa fa-upload"></i> Save Changes and Upload Images</button>
    </fieldset>
    </form>
</div>
@section("scriptlinks")
<script type="text/javascript" src="{{ URL::asset('js/editemp.js') }}"></script>
@endsection

<!-- <script type="text/javascript">
    $( document ).ready(function() {
        $('#countryid').change(function(){
        var countryID = $(this).val();    
        if(countryID){
            $.ajax({
            type:"GET",
            url:"{{url('get-state-list')}}?country_id="+countryID,
            alert(url);
            success:function(res){               
                if(res){
                    $("#stateid").empty();
                    $("#stateid").append('<option>Select</option>');
                    $.each(res,function(key,value){
                        $("#stateid").append('<option value="'+key+'">'+value+'</option>');
                    });
            
                }else{
                $("#stateid").empty();
                }
            }
            });
        }else{
            $("#stateid").empty();
            $("#stateid").empty();
        }      
        });
    });
</script> -->