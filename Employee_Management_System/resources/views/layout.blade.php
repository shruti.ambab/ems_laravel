<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield("title","EMS | Home Page")</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    @yield("csslinks")
</head>
<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
        <a class="navbar-brand" href="/">Employee Management System</a>
    
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active" >
                <a class="nav-link" href="/"> Home </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/organizations"> Manage Organizations </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/employees"> Manage Employees </a>
            </li>
        </ul>
    </nav>
    <div class="container">
        @yield("content")
    </div>  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> 
@yield("scriptlinks")
</body>
</html> 