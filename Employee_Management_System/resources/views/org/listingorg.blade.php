@extends("layout")
@section("title","EMS | Manage Organizations")
@section("csslinks")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
@section("content")
<div style="margin-top:5em;margin-bottom:5em">
    <div ><h4>Manage Organizations</h4></div>
    <hr>
    <button type="button" class="btn btn-dark" id="searchbtn">Search with Filter <i class="fa fa-filter"></i></button>
    <a href="/organizations/create" type=button class="btn btn-primary new pull-right" style="float:right">Add New Organization</a>
    <div class="clearfix"></div>
    <br>
    <form id="filterform" action="/organizations/search">
    {{ csrf_field() }}
        <div id="filter" class="d-flex justify-content-between" style="display: none !important;border: 1px solid #000;padding: 20px;border-radius: 20px">
            <div >
                <span>Organization Name : </span>
                <select id ="org_id" name ="org_id" >
                    <option value="" selected='true'>Select Organization</option>
                    @foreach($orglist as $value)
                        <option id="{{ $value['org_id'] }}" value="{{ $value['org_id'] }}">{{ $value['org_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div>
                <span>Country : </span>
                <select id ="country_id" name ="country_id" >
                    <option value="" selected='true'>Select Country</option>
                    @foreach($countrylist as $value)
                        <option id="{{ $value['country_id'] }}" value="{{ $value['country_id'] }}">{{ $value['country_name'] }}</option>
                    @endforeach
                </select>
            </div>
            <div class="align-self-center">
                <input type="submit" id="searchsubmit" name="searchsubmit" class="btn btn-secondary" value="Search"/>
            </div>
        </div>
    </form>
    @if($message=Session::get('success'))
            <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
    @endif
    @if($message=Session::get('error'))
            <div class="alert alert-danger alert-block">
            <button type="button" class="close" data-dismiss="alert">x</button>
            <strong> {{ $message }} </strong>
            </div>
    @endif
    <table class="table table-bordered table-hover" id="listingorg">
        <thead >
            <tr class="table-primary">
                <th onclick="sortTable(0)">Organization ID</th>
                <th onclick="sortTable(1)">Organization Name</th>
                <th onclick="sortTable(2)">Country</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach($data as $items) : ?>
            <tr>         
                <td onclick="window.location='/organizations/{{ $items['org_id'] }}';"><?php echo $items["org_id"]; ?></td>
                <td onclick="window.location='/organizations/{{ $items['org_id'] }}';"><?php echo $items["org_name"]; ?></td>
                <td onclick="window.location='/organizations/{{ $items['org_id'] }}';"><?php echo $items["country_name"]; ?></td>
                <td>
                <div class="btn-group">
                    <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Choose
                    </button>
                    <div class="dropdown-menu">
                        <div class="dropdown-item">
                            <a href="/organizations/{{ $items['org_id'] }}/edit" class="btn btn-default">Edit</a>
                        </div>
                        <form class="dropdown-item" action="/organizations/{{ $items['org_id'] }}" method="post">
                            <input class="btn btn-default" type="submit" value="Delete" />
                            @method('delete')
                            @csrf
                        </form>
                    </div>
                </div>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="d-flex justify-content-center">{{$data->appends(Request::except('page'))->links()}}</div>
</div>
@endsection
@section("scriptlinks")
<script type="text/javascript" src="{{ URL::asset('js/listingorg.js') }}"></script>
@endsection