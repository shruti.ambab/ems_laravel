@extends("layout")
@section("title","EMS | Edit Organization")
@section("content")
<!-- <p style="margin-top:5em">{{ $orgdata["org_id"] }}</p> -->
<div class="container" style="margin-top:5em">
    <form method="POST" action="/organizations/{{ $orgdata['org_id'] }}">
    {{ csrf_field() }}
    {{ method_field("PUT") }}
    <fieldset class="fieldset">
        <legend class="legend"><span>Edit Organization (ID-{{ $orgdata["org_id"] }})</span></legend>
        <hr>
        <div class="form-group">
            <label >Organization Name</label>
            <input type="text" name="orgname" class="form-control" id="org" placeholder="Enter Organization Name" value="{{ $orgdata['org_name'] }}">  
        </div>
        <div class="form-group">
            <label >Country</label>
            <select class="form-control" id ="countryid" name ="countryid">
                @foreach($countrylist as $value)
                @if($orgdata['country_id'] == $value['country_id'])
                    <option value="{{ $value['country_id'] }}" selected="true">{{ $value['country_name'] }}</option>
                @else
                    <option value="{{ $value['country_id'] }}">{{ $value['country_name'] }}</option>

                @endif
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </fieldset>
    </form>
</div>
@endsection

 <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->