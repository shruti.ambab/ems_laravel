@extends("layout")
@section("title","EMS | Add Organization")
@section("content")
<div class="container" style="margin-top:5em">
    <form method="POST" action="/organizations">
    {{ csrf_field() }}
    <fieldset class="fieldset">
        <legend class="legend"><span>Add New Organization</span></legend>
        <hr>
        <div class="form-group">
            <label >Organization Name</label>
            <input type="text" name="orgname" class="form-control" id="org" placeholder="Enter Organization Name">
            <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
        </div>
        <div class="form-group">
            <label >Country</label>
            <select class="form-control" name="countryid">
                @foreach ($countrylist as $key => $items)
                <option value="{{ $items['country_id'] }}">{{ $items['country_name'] }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
    </fieldset>
    </form>
</div>
@endsection