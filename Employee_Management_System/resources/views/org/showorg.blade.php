@extends("layout")
@section("title",'Organization Details')
@section("content")
<div class="container" style="margin-top:5em">
    <div style="float:left"><h4>Organization (ID-{{ $output["org_id"] }}) Details</h4></div>
    <div class="clearfix"></div>
    <br>
    <ul class="list-group">
        <li class="list-group-item "><b>ID : </b>{{ $output["org_id"] }}</li>
        <li class="list-group-item"><b>Name : </b>{{ $output["org_name"] }}</li>
        <li class="list-group-item"><b>Country : </b>{{ $output["country_name"] }}</li>
    </ul><br>
    <div>
        <a style="margin-bottom:1em" href="/organizations/{{ $output['org_id'] }}/edit" type=button class="btn btn-success new">Edit</a>
        <!-- <a href="" type=button class="btn btn-danger new">Delete</a> -->
        <form action="/organizations/{{ $output['org_id'] }}" method="post">
            <input class="btn btn-danger" type="submit" value="Delete" />
            @method('delete')
            @csrf
        </form>
    </div>
</div>
@endsection