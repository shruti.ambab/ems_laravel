<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/organizations','emsapi\organizationController@index');//listing api (show all records)

Route::get('/employees','emsapi\employeeController@index');

Route::get('/employees','emsapi\employeeController@index');

Route::post('/organizations/addorganization','emsapi\organizationController@store');

Route::post('/employees/addemployee','emsapi\employeeController@store');// add listing page url part b4 add employee if control is going from there to addemployee page

Route::get('/organizations/{id}','emsapi\organizationController@show');

Route::get('/employees/{id}','emsapi\employeeController@show');

Route::put('/organizations/{id}','emsapi\organizationController@update');

Route::put('/employees/{id}','emsapi\employeeController@update');

Route::delete('/organizations/{id}','emsapi\organizationController@destroy');

Route::delete('/employees/{id}','emsapi\employeeController@destroy');