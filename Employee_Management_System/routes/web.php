

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/organizations','ems\organizationController@index');

Route::get('/employees','ems\employeeController@index');

Route::get('/employees/search','ems\employeeController@filter');

Route::get('/organizations/search','ems\organizationController@filter');

// Route::get('/employees/search',function () {
//     return view('home');
// });

Route::get('/organizations/create','ems\organizationController@create');//show create form

Route::get('/employees/create','ems\employeeController@create');

Route::get('/employees/{id}','ems\employeeController@show')->where('id', '[0-9]+');

Route::get('/organizations/{id}','ems\organizationController@show')->where('id', '[0-9]+');

Route::post('/organizations','ems\organizationController@store');// save create form data

Route::post('/employees','ems\employeeController@store');

Route::get('/employees/{id}/edit','ems\employeeController@edit'); //show edit form

Route::get('/organizations/{id}/edit','ems\organizationController@edit');

Route::put('/employees/{id}','ems\employeeController@update'); //save edit form data

Route::put('/organizations/{id}','ems\organizationController@update');

Route::delete('/organizations/{id}','ems\organizationController@destroy');

Route::delete('/employees/{id}','ems\employeeController@destroy');







