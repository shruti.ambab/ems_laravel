$(document).ready(function(){
    $("#add").click(function(){
        let picscount = document.getElementById("picscount").value;
        if(picscount==3){
            $("#filemsg").html("Maximum 3 pictures allowed. Can not add more.");
            $("#filemsg").css({ "color":"red" });
        }
        else{
            document.getElementById("upload").style.display = "block";
            $("#save").click(function(){     
                let picscount = document.getElementById("picscount").value;
                let filescount=$("#profilepics").get(0).files.length;
                if(filescount==0){
                    $("#uploadfilemsg").html("Chosse files first.");
                    $("#uploadfilemsg").css({ "color":"red" });
                    return false;
                }
                for (let i = 0; i < $("#profilepics").get(0).files.length; ++i)
                {
                    let file=$("#profilepics").get(0).files[i].name;
                    if(file){                        
                        let ext = file.split('.').pop().toLowerCase();                            
                        if($.inArray(ext,['jpg','jpeg','png'])===-1){
                            $("#uploadfilemsg").html("Invalid file extension. Only .png, .jpg, .jpeg files allowed.");
                            $("#uploadfilemsg").css({ "color":"red" });
                            return false;
                        }
                    }
                }
                //console.log(parseInt(filescount)+parseInt(picscount));        
                if((parseInt(filescount)+parseInt(picscount))>3){
                    $("#uploadfilemsg").html("In total maximum 3 picture are allowed. Delete old pictures if you want to add new ones.");
                    $("#uploadfilemsg").css({ "color":"red" });
                    return false;
                }
                if(picscount==0){
                    let avatarfilename =  $("#profilepics").get(0).files[0].name;
                    let x = document.getElementById("frontpicfile").value;
                    document.getElementById("frontpicfile").value = avatarfilename;
                }  
        });
    }
}); 
$("#avatar").click(function(){
    //let picscount = document.getElementById("picscount").value;
    // let i=0;
    // for(i=1;i<=picscount;i++){
    //     let name = "pic";
    //     name += i;
    //     console.log(name);
    //     if(document.getElementById(name).checked==true){
    //         //let newfrontpic = document.getElementById(name).alt;
    //         //console.log( $(this).children("img").attr("alt"));
    //         console.log(document.getElementById(name));
    //         //document.getElementById("frontpicfile").value = newfrontpic;
    //     }
    // }
    let picnames=$("input:checkbox");
    let images =$("img");
    //console.log(images[0]);
    //console.log($(picnames[0]).attr("id"));
    let picscount = document.getElementById("picscount").value;
    let i=0;
    let count =0;
    for(i=0;i<picscount;i++){
        if($(picnames[i]).prop('checked')){
            count++;
        }
    }
    if(count==0){
        $("#filemsg").html("Select an image first.");
        $("#filemsg").css({ "color":"red" });
    }
    else if(count>1){
        $("#filemsg").html("Only one image can be set as avatar. Select only one.");
        $("#filemsg").css({ "color":"red" });
    }
    else{
        for(i=0;i<picscount;i++){
            //console.log($(picnames[i]));
            //let picname=$(picnames[i]).attr("id");
            //console.log($(picnames[i]));
            let newfrontpic = $(images[i]).attr("alt");;
            //console.log(newfrontpic);
            if($(picnames[i]).prop('checked')){
                document.getElementById("frontpicfile").value = newfrontpic;
                $("#filemsg").html("Picture selected to be set as avatar. Please save to persist it.");
                $("#filemsg").css({ "color":"green" });
            }
            //console.log(picname);
            //console.log($("#"+picname).val());
            //if($(picname).prop('checked')){
                //console.log(picname);
                // $("#frontpicfile").val(picname);
                // $("#filemsg").html("Picture selected to be set as avatar. Please save to persist it");
                // $("#filemsg").css({ "color":"green" });
            //}
        }
    }
});
$("#delete").click(function(){
    let picnames=$("input:checkbox");
    let images =$("img");
    let picscount = document.getElementById("picscount").value;
    let count =0;
    let i=0;
    for(i=0;i<picscount;i++){
        if($(picnames[i]).prop('checked')){
            count++;
        }
    }
    if(count==0){
        $("#filemsg").html("Select an image first.");
        $("#filemsg").css({ "color":"red" });
    }
    else{
        $("#filemsg").html("Picture selected to be deleted. Please save to persist it.");
        $("#filemsg").css({ "color":"green" });
        let dfiles=[];
        for(i=0;i<picscount;i++){
            let picname = $(images[i]).attr("alt");;
            if($(picnames[i]).prop('checked')){
                //document.getElementById("deletefile").value = picname;
                //let elem = $('input[name=deletefiles\\[\\]]');
                //console.log(elem);
                //elem.val( elem.val() + picname );
                dfiles.push(picname);
            }
        }
        //console.log(dfiles);
        $('[name=deletefiles]').val(dfiles);
    }
});
});