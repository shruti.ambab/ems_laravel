<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;

class countryM extends Model
{
    protected $table = "countries";
    protected $primaryKey = "country_id";
    public function states()
    {
        return $this->hasMany('App\ems\stateM');
    }
    public function cities()
    {
        return $this->hasMany('App\ems\cityM');
    }
}
