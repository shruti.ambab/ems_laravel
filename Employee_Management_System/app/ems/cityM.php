<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;

class cityM extends Model
{
    protected $table = "cities";
    protected $primaryKey = "city_id";
    public function state()
    {
        return $this->belongsTo('App\ems\stateM');
    }
    public function country()
    {
        return $this->belongsTo('App\ems\countryM');
    }
}
