<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;

class stateM extends Model
{
    protected $table = "states";
    protected $primaryKey = "state_id";
    public function country()
    {
        return $this->belongsTo('App\ems\countryM');
    }
}
