<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;

class organizationM extends Model
{
    protected $table = "organizations";
    protected $fillable = ["org_name","country_id"      
    ];
    protected $primaryKey = "org_id";
    public function employees()
    {
        return $this->hasMany('App\ems\employeeM');
    }
}
