<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class employeeM extends Model
{
    protected $table = "employees";
    protected $fillable = ["org_id","fname","lname","contact","local_add","city_id","state_id","country_id","is_enabled"   
    ];
    protected $primaryKey = "emp_id";
    public function organization()
    {
        return $this->belongsTo('App\ems\organizationM');
    }
    public function country()
    {
        return $this->belongsTo('App\ems\countryM');
    }
    public function state()
    {
        return $this->belongsTo('App\ems\stateM');
    }
    public function city()
    {
        return $this->belongsTo('App\ems\cityM');
    }
}
