<?php

namespace App\ems;

use Illuminate\Database\Eloquent\Model;

class profilepicM extends Model
{
    protected $table = "profilepics";
    protected $fillable = ["emp_id","pic","flag"     
    ];
    protected $primaryKey = "pic_id";
    public function getImagePath()
    {
        return $this->pic;
    }
}
