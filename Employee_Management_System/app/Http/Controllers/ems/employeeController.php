<?php

namespace App\Http\Controllers\ems;

use Illuminate\Http\Request;
use App\ems\countryM;
use App\ems\stateM;
use App\ems\cityM;
use App\ems\organizationM;
use App\ems\employeeM;
use File;
use Illuminate\Support\Facades\DB;
// use DataTables;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;


class employeeController extends Controller
{
    public function filter(Request $request){
        //echo"hi";
        //echo"<pre>";var_dump($request->all());
        $query = employeeM::select('*');
        //$query = $query->newQuery();
        // $empid=$request->input('empid');
        // $fname=$request->input('fname');
        // $lname=$request->input('lname');
        // $orgid=$request->input('orgid');
        // $email=$request->input('email');
        // $contact=$request->input('contact');
        // $localadd=$request->input('localadd');
        // $countryid=$request->input('countryid');
        // $stateid=$request->input('stateid');
        // $cityid=$request->input('cityid');
        // $status=$request->input('status');
        $query->where('is_enabled', $request->input('is_enabled'));
        $contact=$request->input('contact');
        if(!empty($contact)){
            $query->where('contact', $request->input('contact'));
        }
        $fields = ['emp_id','org_id','email','contact','fname','lname','local_add','city_id','state_id','country_id'];
        foreach($fields as $field){
            if(!empty($request->$field)){
                //echo"<pre>";var_dump($request->$field);exit;
                //echo $field;
                $query->where($field,'LIKE', '%'.$request->$field.'%');
            }
        }
        //exit;
        // if(!empty($status)){
        //     $query->where('is_enabled', $request->input('status'));
        // }
        // if(!empty($empid)){
        //     //echo "bye";exit;
        //     $query->where('emp_id', $request->input('empid'));
        // }
        // if(!empty($fname)){
        //     $query->where('fname', $request->input('fname'));
        // }
        // if(!empty($lname)){
        //     $query->where('lname', $request->input('lname'));
        // }
        // if(!empty($orgid)){
        //     //echo "hii";exit;
        //     $query->where('org_id', $request->input('orgid'));
        //     //dd($employee->get());
        // }
        // if(!empty($email)){
        //     $query->where('email', $request->input('email'));
        // }
        // if(!empty($contact)){
        //     $query->where('contact', $request->input('contact'));
        // }
        // if(!empty($localadd)){
        //     $query->where('local_add', $request->input('localadd'));
        // }
        // if(!empty($countryid)){
        //     $query->where('country_id', $request->input('countryid'));
        // }
        // if(!empty($stateid)){
        //     $query->where('state_id', $request->input('stateid'));
        // }
        // if(!empty($cityid)){
        //     $query->where('city_id', $request->input('cityid'));
        // }
        //echo(gettype($query));exit;
        $data=$query->get();
        //echo(gettype($data));exit;
        $data = json_decode(json_encode($data), true); 
        //echo"<pre>";var_dump($data);exit;
        //echo(count($data));exit;
        $i=0;
        if($data)
        {
            for($i=0;$i<count($data);$i++)
            {
                $data[$i]["org_name"]= DB::table('organizations')->where('org_id', $data[$i]["org_id"])->value('org_name');
                $data[$i]["country_name"]= DB::table('countries')->where('country_id', $data[$i]["country_id"])->value('country_name');
                $data[$i]["state_name"]= DB::table('states')->where('state_id', $data[$i]["state_id"])->value('state_name');
                $data[$i]["city_name"]= DB::table('cities')->where('city_id', $data[$i]["city_id"])->value('city_name');
                $data[$i]["frontprofilepic"]= DB::table('profilepics')->where('emp_id', $data[$i]["emp_id"])->where('flag', 1)->value('pic');
            }
        }
        //echo"<pre>";var_dump($data);exit;
        $orglist=organizationM::all()->toArray();
        $countrylist=countryM::all()->toArray();
        $statelist=stateM::all()->toArray();
        $citylist=cityM::all()->toArray();
        //echo(gettype($data));exit;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();  //Get current page form url e.g. &page=6
        $collection = new Collection($data); //Create a new Laravel collection from the array data
        $per_page = 3; //Define how many items we want to be visible in each page
        $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();//Slice the collection to get the items to display in current page
        $data = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);
        $data->setPath($request->url()); 
        //$data->withPath('/employees');
        //echo"<pre>";var_dump($data);exit;
        return view('emp/listingemp',compact("data","countrylist","statelist","citylist","orglist"));
        
    }
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url="http://ems.com/api/employees";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $data= curl_exec($ch);
        curl_close($ch);
        $data=json_decode($data,true);
        //echo gettype($data); 
        // echo"<pre>";var_dump($data);
        // var_dump(($data[0])->fname);
        // exit;
        //$data->emp_id;exit;
        //Array data you want to paginate
        $orglist=organizationM::all()->toArray();
        $countrylist=countryM::all()->toArray();
        $statelist=stateM::all()->toArray();
        $citylist=cityM::all()->toArray();
        //echo "<pre>";var_dump ($data);exit;
        //echo gettype($output);exit;
        // echo $output;exit;
        // $i=0;
        // if (is_array($output) || is_object($output))
        // {
        //     for($i=0;$i<count($output);$i++){
        //         $output[$i]["org_id"]=DB::table('organizations')->where('org_id', $output[$i]["org_id"])->value('org_name');

        //         if($output[$i]["is_enabled"]==1){$output[$i]["is_enabled"]="Yes";}
        //         else if($output[$i]["is_enabled"]==0){$output[$i]["is_enabled"]="No";}

        //         $output[$i]["city_id"]=DB::table('cities')->where('city_id', $output[$i]["city_id"])->value('city_name');

        //         $output[$i]["state_id"]=DB::table('states')->where('state_id', $output[$i]["state_id"])->value('state_name');

        //         $output[$i]["country_id"]=DB::table('countries')->where('country_id', $output[$i]["country_id"])->value('country_name');

           
        //     }
        //     //echo "<pre>";var_dump ($output);exit;
        // }
        // $output = Product::sortable()->paginate(5);
        // $paginator = new Paginator($output, $count, $limit, $page, [
        //     'path'  => $this->request->url(),
        //     'query' => $this->request->query(),
        // ]);
        
        
        // $paginate = 5;
        // $page = Input::get('page', 1);
        

        // $offSet = ($page * $paginate) - $paginate;  
        // $itemsForCurrentPage = array_slice($output, $offSet, $paginate, true);  
        // $result = new \Illuminate\Pagination\LengthAwarePaginator($itemsForCurrentPage, count($output), $paginate, $page);
        // //echo gettype($output);exit;
        // //echo "<pre>";var_dump ($output);exit;
        // $output = $result->toArray();
        // //echo "<pre>";var_dump ($output);exit;

        //$data = array();  //This would contain all data to be sent to the view
        $currentPage = LengthAwarePaginator::resolveCurrentPage();  //Get current page form url e.g. &page=6
        $collection = new Collection($data); //Create a new Laravel collection from the array data
        $per_page = 3; //Define how many items we want to be visible in each page
        $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();//Slice the collection to get the items to display in current page
        $data = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);
        $data->setPath($request->url()); //Set base url for pagination links to follow e.g custom/url?page=6
        //echo "<pre>";var_dump ($data);exit;
        // return Datatables::of($data)->addColumn('name', function($row){
        //     return $row["fname"].$row["lname"];})
        //     ->addColumn('address', function($row){
        //     return $row["local_add"].$row["city_name"].$row["state_name"].$row["country_name"];})->addColumn('profilepic', function ($row) { $url=asset('images/'.$row['frontprofilepic']); return '<img src='.$url.' style="width:70px;height:70px;" alt="$row->frontprofilepic" />'; })
        //     ->addColumn('action', function($row) {
        //         return '<a href="/employees/'. $row["emp_id"] .'/edit" class="btn btn-primary">Edit</a>';
        //     })->editColumn('delete', function ($row) {
        //         return '<a href="/employees/'. $row["emp_id"].'">Delete</a>';
        //     })->rawColumns(['delete' => 'delete','action' => 'action'])
        //     ->make(true);
        return view('emp/listingemp',compact("data","countrylist","statelist","citylist","orglist"));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo"hii";exit;
        $orglist=organizationM::all()->toArray();
        $countrylist=countryM::all()->toArray();
        $statelist=stateM::all()->toArray();
        $citylist=cityM::all()->toArray();
       
        return view('emp/addemp',compact("countrylist","statelist","citylist","orglist"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo"<pre>";var_dump($request->all());exit;
        //echo"hii";
        //echo request("frontpicfile");exit;
        $flagonefile = request("frontpicfile");
        //echo"<pre>";var_dump($request->file('profilepics'));
        //$pics = $request->file('profilepics');
        // if($request->hasfile('profilepics')){
        //     //echo"<pre>";var_dump($request->file('profilepics'));
        //     //$pics = $request->file('profilepics');
        //     foreach($request->file('profilepics') as $file)
        //     {
        //         $name=$file->getClientOriginalName();
        //         //echo $name;
        //         $file->move(public_path().'/images/', $name);  
        //         $names[] = $name;  
        //     }
        // }
        //var_dump($data); exit;
        //exit;
        if($request->hasfile('profilepics')){
            foreach($request->file('profilepics') as $file){
                //echo"<pre>";var_dump($picfiles);
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/images/', $name);  
                $names[] = $name;  
            }
        }
        //var_dump($names);exit;
        $data;
        $orgid=request("orgid");
        $fname=request("fname");
        $lname=request("lname");
        $emailid=request("emailid");
        $contact=request("contact");
        $localadd=request("localadd");
        $countryid=request("countryid");
        $stateid=request("stateid");
        $cityid=request("cityid");
        $enability=request("status");
        if($request->hasfile('profilepics')){
            $data = [ "org_id" => $orgid, "fname" => $fname, "lname" => $lname, "email" => $emailid, "contact" => $contact, "local_add" => $localadd, "city_id" => $cityid, "state_id" => $stateid, "country_id" => $countryid, "is_enabled" => $enability, "flagonefile" => $flagonefile, "picnames" => $names];
        }
        else{
            $data = [ "org_id" => $orgid, "fname" => $fname, "lname" => $lname, "email" => $emailid, "contact" => $contact, "local_add" => $localadd, "city_id" => $cityid, "state_id" => $stateid, "country_id" => $countryid, "is_enabled" => $enability, "flagonefile" => $flagonefile, "picnames" => NULL];

        }
        //echo"<pre>";var_dump($pics);exit;
        //echo"<pre>";var_dump($pics);exit;
        //echo"<pre>";var_dump($data);exit;
        //echo gettype($data);exit;
        $data=json_encode($data);
        //echo gettype($data);echo gettype($pics);exit;
       
        //echo"<pre>";var_dump($picfiles);
        // //exit;
        // foreach($pics as $pic){
        //     $this->_toJson($pic);
        // }
        // echo"<pre>";var_dump($pics[0]);exit;
        //echo gettype($pics);exit;
        // $data=array_merge($data,$pics);
        //echo"<pre>";var_dump($data);exit;
        //echo gettype($data);exit;
        // echo $data["0"];exit;
      
        //echo"<pre>";var_dump($data);exit;
        $url="http://ems.com/api/employees/addemployee";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);exit;
        

        if($output['status']=="success"){
            $empid=$output["emp_id"];
            return redirect('/employees')->with('success','Employee (ID-'.$empid.') saved successfully');
        }
        else if($output['status']=="error"){
            return redirect('/employees/create')->with('error',$output['message']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($emp)
    {
        $url="http://ems.com/api/employees/$emp";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);
        return view('emp/showemp',compact("output"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($empid)
    {
        //echo"hiedit";
        $url="http://ems.com/api/employees/$empid";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $empdata=json_decode($output,true);
        //echo"<pre>";var_dump($orgdata);exit;
        
        $orglist=organizationM::all()->toArray();
        $countrylist=countryM::all()->toArray();
        $statelist=stateM::all()->toArray();
        $citylist=cityM::all()->toArray();
        
        return view('emp/editemp',compact("countrylist","statelist","citylist","orglist","empdata"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $empid)
    {
        //echo "hii";
        //echo"<pre>";var_dump($request->file('profilepics'));
        //echo"<pre>";var_dump($request->all());exit;
        // $deletefiles = request("deletefiles");
        // if(!empty($deletefiles)){
        //     echo "its not null";exit;
        // }
        // else{
        //     echo "its null";exit;
        // }
        //$deletefiles->toArray();
        //echo"<pre>";var_dump($deletefiles);exit;
        $flagonefile = request("frontpicfile");
        //echo ($flagonefile);exit;
        if($request->hasfile('profilepics')){
            foreach($request->file('profilepics') as $file){
                //echo"<pre>";var_dump($picfiles);
                $name=$file->getClientOriginalName();
                $file->move(public_path().'/images/', $name);  
                $names[] = $name;  
            }
        }
        $data;
        $orgid=request("orgid");
        $fname=request("fname");
        $lname=request("lname");
        $emailid=request("emailid");
        $contact=request("contact");
        $localadd=request("localadd");
        $countryid=request("countryid");
        $stateid=request("stateid");
        $cityid=request("cityid");
        $enability=request("status");
        $deletefiles = request("deletefiles");
        
        if($request->hasfile('profilepics')){
            $data = [ "org_id" => $orgid, "fname" => $fname, "lname" => $lname, "email" => $emailid, "contact" => $contact, "local_add" => $localadd, "city_id" => $cityid, "state_id" => $stateid, "country_id" => $countryid, "is_enabled" => $enability, "flagonefile" => $flagonefile, "picnames" => $names];
        }
        else if(!empty($deletefiles)){
            //echo gettype($deletefiles);exit;
            $deletefiles=explode (",", $deletefiles);  
            //echo gettype($deletefiles);exit;
            foreach($deletefiles as $deletefile){
                $image_path = public_path().'/images/'.$deletefile;
                // if(File::exists($image_path)) {
                //     File::delete($image_path);
                // }
                if (file_exists($image_path)) {
                    @unlink($image_path);             
                }            
            }
            

            $data = [ "org_id" => $orgid, "fname" => $fname, "lname" => $lname, "email" => $emailid, "contact" => $contact, "local_add" => $localadd, "city_id" => $cityid, "state_id" => $stateid, "country_id" => $countryid, "is_enabled" => $enability, "flagonefile" => $flagonefile, "deletefiles" => $deletefiles];
        }
        else{
            $data = [ "org_id" => $orgid, "fname" => $fname, "lname" => $lname, "email" => $emailid, "contact" => $contact, "local_add" => $localadd, "city_id" => $cityid, "state_id" => $stateid, "country_id" => $countryid, "is_enabled" => $enability, "flagonefile" => $flagonefile];
        }
        $data=json_encode($data);
        //echo"<pre>";var_dump($data);exit;
        $url="http://ems.com/api/employees/$empid";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);exit;

        //echo gettype((int)$empid);exit;
       

        if($output['status']=="success"){
            return redirect()->back()->with('success','Employee (ID-'.$empid.') updated successfully');
          
        }
        else if($output['status']=="error"){
            return redirect()->back()->with('error',$output['message']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($emp)
    {
        $url="http://ems.com/api/employees/$emp";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);
        //echo"<pre>";var_dump($output);
        if($output['status']=="success"){
            return redirect('/employees')->with('success','Employee (ID-'.$emp.') soft deleted successfully');
        }
        else{
            return redirect('/employees')->with('error','Error: '.$output['status']);
        }
    }
}
