<?php

namespace App\Http\Controllers\ems;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ems\countryM;
use App\ems\organizationM;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class organizationController extends Controller
{
    public function filter(Request $request){
        //echo"hi";
        //echo"<pre>";var_dump($request->all());exit;
        $query = organizationM::select('*');
        $fields = ['org_id','country_id'];
        foreach($fields as $field){
            if(!empty($request->$field)){
                //echo"<pre>";var_dump($request->$field);exit;
                //echo $field;
                $query->where($field,'LIKE', '%'.$request->$field.'%');
            }
        }
        //exit;
        //echo(gettype($query));exit;
        $data=$query->get();
        //echo(gettype($data));exit;
        $data = json_decode(json_encode($data), true); 
        //echo"<pre>";var_dump($data);exit;
        //echo(count($data));exit;
        $i=0;
        if($data)
        {
            for($i=0;$i<count($data);$i++)
            {
                $data[$i]["org_name"]= DB::table('organizations')->where('org_id', $data[$i]["org_id"])->value('org_name');
                $data[$i]["country_name"]= DB::table('countries')->where('country_id', $data[$i]["country_id"])->value('country_name');
            }
        }
        //echo"<pre>";var_dump($data);exit;
        $countrylist=countryM::all()->toArray();
        $orglist=organizationM::all()->toArray();
        //echo(gettype($data));exit;
        $currentPage = LengthAwarePaginator::resolveCurrentPage();  //Get current page form url e.g. &page=6
        $collection = new Collection($data); //Create a new Laravel collection from the array data
        $per_page = 3; //Define how many items we want to be visible in each page
        $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();//Slice the collection to get the items to display in current page
        $data = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);
        $data->setPath($request->url()); 
        //$data->withPath('/employees');
        //echo"<pre>";var_dump($data);exit;
        return view('org/listingorg',compact("data","countrylist","orglist"));
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $url="http://ems.com/api/organizations";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);
        //echo "<pre>";var_dump ($output);exit;
        // echo gettype($output);exit;
        // echo $output;exit;
        // $i=0;
        // if (is_array($output) || is_object($output))
        // {
        //     // foreach($output as $data){
        //     // //echo "<pre>";var_dump ($data);
        //     // $country_id=$data["country_id"];
        //     // $country=countryM::find($country_id);
        //     // $country_name=$country["country_name"];
        //     // //echo (organizationM::all()->country());exit;
        //     // //echo( $country_name);
        //     // //echo($country_id);
        //     // $data["country_id"]= $country_name;
        //     // echo "<pre>";var_dump ($output);exit;
        //     // //echo ($data["country_id"]);
        //     // }
            
        //     // for($i=0;$i<count($output);$i++){
        //     //     $country_id=$output[$i]["country_id"];
        //     //     $country=countryM::find($country_id);
        //     //     $output[$i]["country_id"]=$country["country_name"];
        //     //     //echo "<pre>";var_dump($output[$i]);
        //     // }
        //     for($i=0;$i<count($output);$i++){
        //         // $country_id=$output[$i]["country_id"];
        //         // $country_name = DB::table('countries')->where('country_id', $country_id)->value('country_name');
        //         // $output[$i]["country_id"]=$country_name;
        //         $output[$i]["country_id"]=DB::table('countries')->where('country_id', $output[$i]["country_id"])->value('country_name');
        //         //echo "<pre>";var_dump($output[$i]);
        //     }
            //echo "<pre>";var_dump ($output);exit;
            
        //}
        $currentPage = LengthAwarePaginator::resolveCurrentPage();  //Get current page form url e.g. &page=6
        $collection = new Collection($output); //Create a new Laravel collection from the array data
        $per_page = 3; //Define how many items we want to be visible in each page
        $currentPageResults = $collection->slice(($currentPage-1) * $per_page, $per_page)->all();//Slice the collection to get the items to display in current page
        $data = new LengthAwarePaginator($currentPageResults, count($collection), $per_page);
        $data->setPath($request->url()); //Set base url for pagination links to follow e.g custom/url?page=6
        $orglist=organizationM::all()->toArray();
        $countrylist=countryM::all()->toArray();
        return view('org/listingorg',compact("data","countrylist","orglist"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //echo "hii";exit;
        $countrylist=countryM::all()->toArray();
        return view('org/addorg',compact("countrylist"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo "hii";exit;
        //return request()->all();
        $data;
        $orgname;
        $countryid;
        $orgname=request("orgname");
        $countryid=request("countryid");
        $data = ["org_name" => $orgname, "country_id" => $countryid];
        $data=json_encode($data);
        //echo"<pre>";var_dump($data);exit;
        $url="http://ems.com/api/organizations/addorganization";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);exit;
        $orgid=$output["org_id"];

        if($output['status']=="success"){
            return redirect('/organizations')->with('success','Organization (ID-'.$orgid.') saved successfully');
        }
        else{
            return redirect('/organizations')->with('error','Error: '.$output['status']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($org)
    {
        $url="http://ems.com/api/organizations/$org";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);
        return view('org/showorg',compact("output"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($orgid)
    {
        //echo "hiedit";
        $countrylist=countryM::all()->toArray();
        $url="http://ems.com/api/organizations/$orgid";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $orgdata=json_decode($output,true);
        //echo"<pre>";var_dump($orgdata);exit;
        return view('org/editorg',compact("countrylist","orgdata"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $orgid)
    {
        //echo"hii";
        $data;
        $orgname;
        $countryid;
        $orgname=request("orgname");
        $countryid=request("countryid");
        $data = ["org_name" => $orgname, "country_id" => $countryid];
        $data=json_encode($data);
        //echo"<pre>";var_dump($data);exit;
        $url="http://ems.com/api/organizations/$orgid";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);

        //echo"<pre>";var_dump($output);exit;
        $orgid=$output["org_id"];

        if($output['status']=="success"){
            return redirect('/organizations')->with('success','Organization (ID-'.$orgid.') updated successfully');
        }
        else{
            return redirect('/organizations')->with('error','Error: '.$output['status']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($org)
    {
        $url="http://ems.com/api/organizations/$org";
        $ch= curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        $output= curl_exec($ch);
        curl_close($ch);
        $output=json_decode($output,true);
        //echo"<pre>";var_dump($output);
        if($output['status']=="success"){
            return redirect('/organizations')->with('success','Organization (ID-'.$org.') deleted successfully');
        }
        else{
            return redirect('/organizations')->with('error','Error: '.$output['status']);
        }
    }
}
