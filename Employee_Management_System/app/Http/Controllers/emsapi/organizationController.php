<?php

namespace App\Http\Controllers\emsapi;

use App\ems\organizationM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class organizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $output = organizationM::all();
        //return $output;exit;

        $i=0;
        if (is_array($output) || is_object($output))
        {
            for($i=0;$i<count($output);$i++){
                $output[$i]->country_name=DB::table('countries')->where('country_id', $output[$i]["country_id"])->value('country_name');
            }
        }
        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $org = new organizationM();
            $org->org_name = $request->input('org_name');
            $org->country_id = $request->input('country_id');

            // $org->country_id=DB::table('countries')->where('country_name', $request->input('country_id'))->value('country_id');

            $org->save();

            $msg = "success";
            $org->status = $msg;
        }
        catch (\Exception $e) {
            $err = $e->getMessage();
            $org->status = "Failed. ".$err;
        }
        return response()->json($org);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\organizationM  $organizationM
     * @return \Illuminate\Http\Response
     */
    public function show($org_id)
    {
        $organization = organizationM::findOrFail($org_id);
        
        $organization->country_name=DB::table('countries')->where('country_id', $organization->country_id)->value('country_name');

        return response()->json($organization);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\organizationM  $organizationM
     * @return \Illuminate\Http\Response
     */
    public function edit(organizationM $organizationM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\organizationM  $organizationM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $org_id) //laravel route-model binding:-you pass the wild card (id of that record) from route and while receiving it in controller you type-paint it with model 
    {
        try{
        $organization = organizationM::findOrFail($org_id);
        
        $organization->org_name = $request->input('org_name');
        $organization->country_id = $request->input('country_id');
        // $organization->country_id=DB::table('countries')->where('country_name', $request->input('country_id'))->value('country_id');

        $organization->save();

        $msg = "success";
        $organization->status = $msg;
        }
        catch (\Exception $e) {
            $err = $e->getMessage();
            $organization->status = "Failed. ".$err;
        }

        //$org->update(request(["org_name","country_id"]));

        return response()->json($organization);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\organizationM  $organizationM
     * @return \Illuminate\Http\Response
     */
    public function destroy($org_id)
    {
        try{
        $organization=organizationM::findOrFail($org_id);
        $organization->delete();

        $msg = "success";
        $organization->status = $msg;
        }
        catch (\Exception $e) {
            $err = $e->getMessage();
            $organization->status = "Failed. ".$err;
        }
        return response()->json($organization);
    }
}
