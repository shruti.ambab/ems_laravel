<?php

namespace App\Http\Controllers\emsapi;

use App\ems\employeeM;
use App\ems\profilepicM;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class employeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $output = employeeM::all();
        //$output=DB::table('employees')->orderBy('emp_id', 'asc')->paginate(5);
        $i=0;
        if (is_array($output) || is_object($output))
        {
            for($i=0;$i<count($output);$i++){
                $output[$i]->org_name=DB::table('organizations')->where('org_id', $output[$i]["org_id"])->value('org_name');

                if($output[$i]["is_enabled"]==1){$output[$i]->is_enabled_name="Enabled";}
                else if($output[$i]["is_enabled"]==0){$output[$i]->is_enabled_name="Disabled";}

                if($output[$i]["is_deleted"]==1){$output[$i]->is_deleted_name="Not Functional";}
                else if($output[$i]["is_deleted"]==0){$output[$i]->is_deleted_name="Functional";}

                $output[$i]->city_name=DB::table('cities')->where('city_id', $output[$i]["city_id"])->value('city_name');

                $output[$i]->state_name=DB::table('states')->where('state_id', $output[$i]["state_id"])->value('state_name');

                $output[$i]->country_name=DB::table('countries')->where('country_id', $output[$i]["country_id"])->value('country_name');

                $output[$i]->frontprofilepic=DB::table('profilepics')->where('emp_id', $output[$i]["emp_id"])->where('flag', 1)->value('pic');
            }
            //echo "<pre>";var_dump ($output);exit;
        }

        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'org_id' => 'required',
            'fname' => 'required|min:2',
            'lname' => 'required|min:2',
            'email' => 'required|email|unique:employees',
            'contact' => 'required|unique:employees|regex:/^[6-9]\d{9}$/',
            'local_add' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'country_id' => 'required',
        ]);
        if ($validator->fails()) {
            //echo"failed";exit;
            // return redirect('/employees')->with('error','Error,Please fill all the fields with valid data');
            $msg =["status" => "error","message" => "Error: Please fill all the fields with valid data."];
            return response()->json($msg);
        }
        $employee = new employeeM();
        try{
            

            // $employee->org_id = DB::table('organizations')->where('org_name', $request->input('org_id'))->value('org_id');
            //echo"<pre>";var_dump($request->all());exit;
            $employee->org_id = $request->input('org_id');
            $employee->fname = $request->input('fname');
            $employee->lname = $request->input('lname');
            $employee->email = $request->input('email');
            $employee->contact = $request->input('contact');
            $employee->local_add = $request->input('local_add');
            // $employee->city_id = DB::table('cities')->where('city_name', $request->input('city_id'))->value('city_id');
            // $employee->state_id = DB::table('states')->where('state_name', $request->input('state_id'))->value('state_id');
            // $employee->country_id = DB::table('countries')->where('country_name', $request->input('country_id'))->value('country_id');
            $employee->city_id = $request->input('city_id');
            $employee->state_id = $request->input('state_id');
            $employee->country_id = $request->input('country_id');
            $employee->is_enabled=$request->input('is_enabled');
            

            //$employee->is_deleted=0;
            
            $employee->save();
            $picnames=$request->input('picnames');
            if(!empty($picnames)){
                foreach($picnames as $picname){

                    $picrecord = new profilepicM();
                    $picrecord->emp_id=$employee->emp_id;
                    $picrecord->pic=$picname;
                    if($request->input('flagonefile')==$picname){
                        $picrecord->flag =1;
                    }
                    $picrecord->save();
                }
            }
            // $file= new File();
            // $file->filenames=$request->input('picnames');
            // $file->save();
            

            $msg = "success";
            $employee->status = $msg;
        }
        catch (\Exception $e) {
            //$err = $e->getMessage();
            $employee->status = "error";
            $employee->message = $e->getMessage();
        }

        return response()->json($employee);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\employeeM  $employeeM
     * @return \Illuminate\Http\Response
     */
    public function show($emp_id)
    {
        $employee = employeeM::findOrFail($emp_id);

        $employee->org_name=DB::table('organizations')->where('org_id', $employee->org_id)->value('org_name');

        $employee->city_name=DB::table('cities')->where('city_id', $employee->city_id)->value('city_name');

        $employee->state_name=DB::table('states')->where('state_id', $employee->state_id)->value('state_name');

        $employee->country_name=DB::table('countries')->where('country_id', $employee->country_id)->value('country_name');

        $employee->frontprofilepic=DB::table('profilepics')->where('emp_id', $emp_id)->where('flag', 1)->value('pic');

        $employee->profilepics=DB::table('profilepics')->where('emp_id', $emp_id)->pluck('pic');
        //return $employee->profilepics;

        $employee->picscount=count($employee->profilepics);

        return response()->json($employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\employeeM  $employeeM
     * @return \Illuminate\Http\Response
     */
    public function edit(employeeM $employeeM)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\employeeM  $employeeM
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $emp_id)
    {
        $employee = employeeM::findOrFail($emp_id);
        $validator = Validator::make($request->all(), [        
            'org_id' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required|email|unique:employees,email,'.(int)$emp_id.',emp_id',
            'contact' => 'required|regex:/^[6-9]\d{9}$/|unique:employees,contact,'.(int)$emp_id.',emp_id',
            'local_add' => 'required'
        ]);
        if ($validator->fails()) {
            $output = ["status" => "error","message" => "Error: Please fill all the fields with valid data."];
            return response()->json($output);
        }
        try{
            

            // $employee->org_id = DB::table('organizations')->where('org_name', $request->input('org_id'))->value('org_id');
            // $employee->fname = $request->input('fname');
            // $employee->lname = $request->input('lname');
            // $employee->contact = $request->input('contact');
            // $employee->local_add = $request->input('local_add');
            // $employee->city_id = DB::table('cities')->where('city_name', $request->input('city_id'))->value('city_id');
            // $employee->state_id = DB::table('states')->where('state_name', $request->input('state_id'))->value('state_id');
            // $employee->country_id = DB::table('countries')->where('country_name', $request->input('country_id'))->value('country_id');
            // if($request->input('is_enabled')=="Yes"){
            //     $employee->is_enabled=1;
            // }
            // else if($request->input('is_enabled')=="No"){
            //     $employee->is_enabled=0;
            // }
            // if($request->input('is_deleted')=="Yes"){
            //     $employee->is_deleted=1;
            // }
            // else if($request->input('is_deleted')=="No"){
            //     $employee->is_deleted=0;
            // }
            $employee->org_id = $request->input('org_id');
            $employee->fname = $request->input('fname');
            $employee->lname = $request->input('lname');
            $employee->email = $request->input('email');
            $employee->contact = $request->input('contact');
            $employee->local_add = $request->input('local_add');
            $employee->city_id = $request->input('city_id');
            $employee->state_id = $request->input('state_id');
            $employee->country_id = $request->input('country_id');
            $employee->is_enabled=(int)$request->input('is_enabled');
            //$employee->is_deleted=0;
        
            $employee->save();
            
            if($request->exists('picnames')){
                foreach($request->input('picnames') as $picname){

                    $picrecord = new profilepicM();
                    $picrecord->emp_id=$emp_id;
                    $picrecord->pic=$picname;
                    $pics_exists=DB::table('profilepics')->where('emp_id',$emp_id)->value('pic_id');
                    if(!(empty($pics_exists))){
                        if($request->input('flagonefile')==$picname){               
                            $pastflagid=DB::table('profilepics')->where('emp_id',$emp_id)->where('flag',1)->value('pic_id');
                            $pastflagrecord=profilepicM::findOrFail($pastflagid);
                            $pastflagrecord->flag=0;
                            $picrecord->flag =1;
                        }
                    }
                    else{
                        $picrecord->flag =1;
                    }
                    $picrecord->save();
                }
            }
           
            if($request->exists('flagonefile')){
                $picnames = DB::table('profilepics')->where('emp_id',$emp_id)->pluck('pic');
                //return $picnames;    
                foreach($picnames as $picname){
                    //return $picname;
                    $picid = DB::table('profilepics')->where('pic',$picname)->value('pic_id');
                    //echo $picid;
                    $picrecord = profilepicM::findOrFail($picid);
                    //echo $picrecord;exit;
                    //echo $request->input('flagonefile');
                    if(($request->input('flagonefile'))==$picname){
                        $picrecord->flag =1;
                    }
                    else{
                        $picrecord->flag =0;
                    }
                    $picrecord->save();
                }
            }
            
            if($request->exists('deletefiles')){
                $deletefiles=$request->input('deletefiles');                
                //$deletefiles=explode (",", $deletefiles);  
                //echo count($deletefiles);exit;
                // echo $deletefiles[0];
                // echo $deletefiles[1];exit;
                $picnames = DB::table('profilepics')->where('emp_id',$emp_id)->pluck('pic');  
                //echo $picnames;exit;
                $frontfiledeleted=0;
                foreach($picnames as $picname){
                    //$picid = DB::table('profilepics')->where('pic',$picname)->value('pic_id');
                    //$picrecord = profilepicM::findOrFail($picid);
                    $i=0;
                    for($i=0;$i<count($deletefiles);$i++){
                        if($picname==$deletefiles[$i]){
                            //echo $deletefiles[$i];
                            
                            $picid = DB::table('profilepics')->where('pic',$picname)->value('pic_id');
                            $picrecord = profilepicM::findOrFail($picid);
                            //echo $picrecord;
                            $picrecord->delete();
                            if($picrecord->flag==1){
                                //echo "1";
                                $frontfiledeleted=1;
                            }
                        }
                    }
                    
                }
                //exit;
                //$frontfiledeleted=1;
                if($frontfiledeleted==1){
                    $rempicids =  DB::table('profilepics')->where('emp_id',$emp_id)->pluck('pic_id');
                    //echo $rempicids[0];exit;
                    if(count($rempicids)!=0){
                        //echo "count not zero"; exit;
                        $firstrem=$rempicids[0];
                        $newfrontpicrecord = profilepicM::findOrFail($firstrem);
                        //echo $newfrontpicrecord;exit;
                        $newfrontpicrecord->flag=1;
                        $newfrontpicrecord->save();
                        //echo $newfrontpicrecord;exit;
                    }
                }
            }

            $employee->status = "success";
        }
        catch (\Exception $e) {
            //$err = $e->getMessage();
            $employee->status = "error";
            $employee->message = $e->getMessage();
        }

        return response()->json($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\employeeM  $employeeM
     * @return \Illuminate\Http\Response
     */
    public function destroy($emp_id)
    {
        $employee=employeeM::findOrFail($emp_id);
        try{
        //$employee=employeeM::findOrFail($emp_id);
        $employee->is_deleted=1;
        $employee->save();

        $msg = "success";
        $employee->status = $msg;
        }
        catch (\Exception $e) {
            //$err = $e->getMessage();
            $employee->status = "Failed. ".$e->getMessage();
        }
        return response()->json($employee);
    }
}
