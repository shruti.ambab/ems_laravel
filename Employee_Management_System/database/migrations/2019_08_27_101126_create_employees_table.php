<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('emp_id');
            $table->bigInteger('org_id')->unsigned();
            $table->string('fname',50);
            $table->string('lname',50);
            $table->string('email')->unique();
            $table->decimal('contact', 10, 0)->unsigned()->unique();
            $table->string('local_add',100);
            $table->bigInteger('city_id')->unsigned();
            $table->bigInteger('state_id')->unsigned();
            $table->bigInteger('country_id')->unsigned();
            $table->foreign('city_id')
                  ->references('city_id')->on('cities')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('state_id')
                  ->references('state_id')->on('states')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('country_id')
                ->references('country_id')->on('countries')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('org_id')
            ->references('org_id')->on('organizations')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->boolean('is_enabled')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
