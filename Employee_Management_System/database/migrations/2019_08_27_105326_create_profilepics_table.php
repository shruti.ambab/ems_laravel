<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilepicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profilepics', function (Blueprint $table) {
            $table->bigIncrements('pic_id');
            $table->bigInteger('emp_id')->unsigned();
            $table->text('pic')->nullable(); //file name
            $table->boolean('flag')->default(0);
            $table->foreign('emp_id')
            ->references('emp_id')->on('employees')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profilepics');
    }
}
