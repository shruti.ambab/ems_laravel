<?php

use Illuminate\Database\Seeder;

class countriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->delete();
        DB::table('countries')->insert(array(
            array(
              'country_name' => 'India'
            ),
            array(
              'country_name' => 'US'
            ),
            array(
              'country_name' => 'Japan'
            )
          ));
    }
       
}
