<?php

use Illuminate\Database\Seeder;

class employeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->delete();
        DB::table('employees')->insert(array(
            array(
              'org_id' => 1,
              'fname' => "shruti",
              'lname' => "pimparkar",
              'email' => "abc@gmail.com",
              'contact' => 8421535261,
              'local_add' => "Yashodhan,patrakar nagar,near civil hospital",
              'city_id' => 1,
              'state_id' => 15,
              'country_id' => 1
            ),
            array(
                'org_id' => 1,
                'fname' => "aarti",
                'lname' => "patel",
                'email' => "xyz@gmail.com",
                'contact' => 9421549261,
                'local_add' => "somewhere in bandra",
                'city_id' => 4,
                'state_id' => 15,
                'country_id' => 1
            ),
            array(
              'org_id' => 4,
              'fname' => "kaustubh",
              'lname' => "pimparkar",
              'email' => "kd@gmail.com",
              'contact' => 9421759261,
              'local_add' => "A-416, Pushpnagar Society",
              'city_id' => 2,
              'state_id' => 15,
              'country_id' => 1
            ),
            array(
              'org_id' => 5,
              'fname' => "shraddha",
              'lname' => "kulkarni",
              'email' => "shraddha95@gmail.com",
              'contact' => 9821759261,
              'local_add' => "A-416, Pushpnagar Society",
              'city_id' => 2,
              'state_id' => 15,
              'country_id' => 1
            ),
            array(
              'org_id' => 6,
              'fname' => "Mayura",
              'lname' => "Kulkarni",
              'email' => "mutai@gmail.com",
              'contact' => 9991759261,
              'local_add' => "Dosti Ambrosia",
              'city_id' => 4,
              'state_id' => 15,
              'country_id' => 1
            )
          ));
    }
}
