<?php

use Illuminate\Database\Seeder;

class profilepicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('profilepics')->delete();
        DB::table('profilepics')->insert(array(
            array(
              'emp_id' => '1',
              'pic' => "empid1_1.jpg",
              'flag' => 1
            ),
            array(
                'emp_id' => '1',
                'pic' => "empid1_2.png",
                'flag' => 0
            ),
            array(
                'emp_id' => '1',
                'pic' => "empid1_3.jpg",
                'flag' => 0
            ),
            array(
                'emp_id' => '2',
                'pic' => "empid2_1.jpg",
                'flag' => 1
            ),
            array(
                'emp_id' => '2',
                'pic' => "empid2_2.jpg",
                'flag' => 0
            ),
            array(
                'emp_id' => '2',
                'pic' => "empid2_3.jpg",
                'flag' => 0
            ), array(
                'emp_id' => '3',
                'pic' => "images (2).jpeg",
                'flag' => 1
            ),
            array(
                'emp_id' => '3',
                'pic' => "default.png",
                'flag' => 0
            ),
            array(
                'emp_id' => '4',
                'pic' => "download.jpeg",
                'flag' => 1
            ),
            array(
                'emp_id' => '4',
                'pic' => "download (5).jpeg",
                'flag' => 0
            ),
            array(
                'emp_id' => '4',
                'pic' => "imagesdddd.png",
                'flag' => 0
            ),
            array(
                'emp_id' => '5',
                'pic' => "download (3).jpeg",
                'flag' => 1
            ),
            array(
                'emp_id' => '5',
                'pic' => "download (2).jpeg",
                'flag' => 0
            )
        ));
    }
}
