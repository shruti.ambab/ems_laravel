<?php

use Illuminate\Database\Seeder;

class citiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->delete();
        DB::table('cities')->insert(array(
            array(
              'city_name' => 'Solapur',
              'state_id' => 15,
              'country_id' => 1
            ),
            array(
                'city_name' => 'Pune',
                'state_id' => 15,
                'country_id' => 1
            ),  
            array(
                'city_name' => 'Banglore',
                'state_id' => 12,
                'country_id' => 1
            ),
            array(
                'city_name' => 'Mumbai',
                'state_id' => 15,
                'country_id' => 1
            ) 
          ));
    }
}
