<?php

use Illuminate\Database\Seeder;

class organizationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('organizations')->delete();
        DB::table('organizations')->insert(array(
            array(
              'org_name' => 'Ambab Infotech',
              'country_id' => 1
            ),
            array(
                'org_name' => 'Infosys',
                'country_id' => 1
                ),
            array(
                'org_name' => 'Precision Camshafts Limited',
                'country_id' => 1
            ),
            array(
                'org_name' => 'Barclays',
                'country_id' => 2
            ),
            array(
                'org_name' => 'Cognizant',
                'country_id' => 1
            ),
            array(
                'org_name' => 'Crompton',
                'country_id' => 1
            )
          ));
    }
}
