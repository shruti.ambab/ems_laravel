<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call('countriesTableSeeder');
        $this->command->info('countries table seeded!');  

        $this->call('statesTableSeeder');
        $this->command->info('states table seeded!');   

        $this->call('citiesTableSeeder');
        $this->command->info('cities table seeded!');   
         
        $this->call('organizationsTableSeeder');
        $this->command->info('organizations table seeded!');  
        
        $this->call('employeesTableSeeder');
        $this->command->info('employees table seeded!');  

        $this->call('profilepicsTableSeeder');
        $this->command->info('profilepics table seeded!');
    }
}
